﻿#define WIN32_LEAN_AND_MEAN

/* Cxi tiuj difinoj normale estas parto de la agordoj de la projekto.
Mi preferis kopii ilin cxi tien, tiel ke oni povas vidi la difinojn
kiuj estas necesaj, sen elsxuti aron da dosieroj por la projekto. */
#ifndef WIN32
# define WIN32
#endif

#ifndef _WINDOWS
# define _WINDOWS
#endif

#ifndef _UNICODE
# define _UNICODE
#endif

#ifndef UNICODE
# define UNICODE
#endif

/* La nuraj necesaj bibliotekoj estas user32 kaj kernel32.
Ni ecx ne bezonas la normajn bibliotekojn de C++! */

#include <windows.h>
#include <tchar.h>

// Eble la subaj difinoj estos utilaj, depende de la malnoveco
// de la .h-dosieroj de Windows kiujn vi uzos.
/*
#ifndef MAPVK_VK_TO_VSC
# define MAPVK_VK_TO_VSC 0
#endif

#ifndef MAPVK_VK_TO_CHAR
# define MAPVK_VK_TO_CHAR 2
#endif 
*/

HINSTANCE hInst;
wchar_t bufro;
UINT backScan;
HHOOK kbdHook;

LRESULT CALLBACK LowLevelKeyboardProc(int code, WPARAM wParam, LPARAM lParam);

inline void InstallHook()
{
    kbdHook = SetWindowsHookEx(WH_KEYBOARD_LL, LowLevelKeyboardProc, hInst, 0);
}

inline void UninstallHook()
{
    UnhookWindowsHookEx(kbdHook);
}

// Ofte, oni ja povas enmeti la esperantajn literojn
// en la fontokodon. Malgraux tio, estas pli fortike
// uzi rekte la unikodajn numerojn cxar ili ne dependas
// de la agordoj de la redaktilo nek de la kompililo.
const wchar_t
    CX = 264, cx = 265,
    GX = 284, gx = 285,
    HX = 292, hx = 293,
    JX = 308, jx = 309,
    SX = 348, sx = 349,
    UX = 364, ux = 365;

inline wchar_t KonvertiEo(wchar_t litero)
{
    switch(litero) {
        case L'C': return CX;     case L'c': return cx;
        case L'G': return GX;     case L'g': return gx;
        case L'H': return HX;     case L'h': return hx;
        case L'J': return JX;     case L'j': return jx;
        case L'S': return SX;     case L's': return sx;
        case L'U': return UX;     case L'u': return ux;
        default:   return litero; // mi devus diri "literon", cxu ne? ;-)
    }
}

// 2013-02-02: malkonverti kiam oni tajpas ikson dufoje
inline wchar_t MalkonvertiEo(wchar_t litero)
{
    switch(litero) {
        case CX: return L'C';     case cx: return L'c';
        case GX: return L'G';     case gx: return L'g';
        case HX: return L'H';     case hx: return L'h';
        case JX: return L'J';     case jx: return L'j';
        case SX: return L'S';     case sx: return L's';
        case UX: return L'U';     case ux: return L'u';
        default: return litero; // mi devus diri "literon", cxu ne? ;-)
    }
}

// Sendi BACKSPACEn plus esperantan literon
void SendiEo(wchar_t litero)
{
    const int LEN = 4;
    INPUT input[LEN];
    input[0].type = INPUT_KEYBOARD;
    input[0].ki.wVk = VK_BACK;
    input[0].ki.wScan = backScan;
    input[0].ki.dwFlags = 0;
    input[0].ki.time = 0;
    input[0].ki.dwExtraInfo = 0;

    input[1].type = INPUT_KEYBOARD;
    input[1].ki.wVk = VK_BACK;
    input[1].ki.wScan = backScan;
    input[1].ki.dwFlags = KEYEVENTF_KEYUP;
    input[1].ki.time = 0;
    input[1].ki.dwExtraInfo = 0;

    input[2].type = INPUT_KEYBOARD;
    input[2].ki.wVk = 0; // por unikodo, uzu cxiam 0
    input[2].ki.wScan = litero;
    input[2].ki.dwFlags = KEYEVENTF_UNICODE;
    input[2].ki.time = 0;
    input[2].ki.dwExtraInfo = 0;

    input[3].type = INPUT_KEYBOARD;
    input[3].ki.wVk = 0; // por unikodo, uzu cxiam 0
    input[3].ki.wScan = litero;
    input[3].ki.dwFlags = KEYEVENTF_UNICODE | KEYEVENTF_KEYUP;
    input[3].ki.time = 0;
    input[3].ki.dwExtraInfo = 0;

    SendInput(LEN, input, sizeof(INPUT));
}

// 2013-02-02: malkonverti kiam oni tajpas ikson dufoje
// Sendi BACKSPACEn plus litero1 plus litero2 (kutime litero2 estas ikso)
void MalsendiEo(wchar_t litero1, wchar_t litero2)
{
    const int LEN = 6;
    INPUT input[LEN];
    input[0].type = INPUT_KEYBOARD;
    input[0].ki.wVk = VK_BACK;
    input[0].ki.wScan = backScan;
    input[0].ki.dwFlags = 0;
    input[0].ki.time = 0;
    input[0].ki.dwExtraInfo = 0;

    input[1].type = INPUT_KEYBOARD;
    input[1].ki.wVk = VK_BACK;
    input[1].ki.wScan = backScan;
    input[1].ki.dwFlags = KEYEVENTF_KEYUP;
    input[1].ki.time = 0;
    input[1].ki.dwExtraInfo = 0;

    input[2].type = INPUT_KEYBOARD;
    input[2].ki.wVk = 0; // por unikodo, uzu cxiam 0
    input[2].ki.wScan = litero1;
    input[2].ki.dwFlags = KEYEVENTF_UNICODE;
    input[2].ki.time = 0;
    input[2].ki.dwExtraInfo = 0;

    input[3].type = INPUT_KEYBOARD;
    input[3].ki.wVk = 0; // por unikodo, uzu cxiam 0
    input[3].ki.wScan = litero1;
    input[3].ki.dwFlags = KEYEVENTF_UNICODE | KEYEVENTF_KEYUP;
    input[3].ki.time = 0;
    input[3].ki.dwExtraInfo = 0;

    input[4].type = INPUT_KEYBOARD;
    input[4].ki.wVk = 0; // por unikodo, uzu cxiam 0
    input[4].ki.wScan = litero2;
    input[4].ki.dwFlags = KEYEVENTF_UNICODE;
    input[4].ki.time = 0;
    input[4].ki.dwExtraInfo = 0;

    input[5].type = INPUT_KEYBOARD;
    input[5].ki.wVk = 0; // por unikodo, uzu cxiam 0
    input[5].ki.wScan = litero2;
    input[5].ki.dwFlags = KEYEVENTF_UNICODE | KEYEVENTF_KEYUP;
    input[5].ki.time = 0;
    input[5].ki.dwExtraInfo = 0;

    SendInput(LEN, input, sizeof(INPUT));
}

// Funkcias por la literoj C, G, H, J, S, U. Tio suficxas.
inline wchar_t SimplaMinuskligo(wchar_t litero)
{
    return litero + 32;
}

bool CxuMajuskle()
{
    bool shiftPremata = (GetKeyState(VK_SHIFT) & 0x80) != 0;
    bool capsLockSxaltita = (GetKeyState(VK_CAPITAL) & 1) != 0;
    return capsLockSxaltita ^ shiftPremata;
}

LRESULT CALLBACK LowLevelKeyboardProc(int code, WPARAM wParam, LPARAM lParam)
{
    if(code == HC_ACTION) {
        KBDLLHOOKSTRUCT* hookStruct = reinterpret_cast<KBDLLHOOKSTRUCT*>(lParam);

        // 2013-02-02: Ignori klavojn kiel Shift kaj Ctrl
        switch(hookStruct->vkCode) {
            case VK_LSHIFT:   case VK_RSHIFT:
            case VK_LMENU:    case VK_RMENU:
            case VK_LCONTROL: case VK_RCONTROL:
            case VK_CAPITAL:  case VK_NUMLOCK:
                return CallNextHookEx(kbdHook, code, wParam, lParam);
            // default: dauxrigi;
        }

        // 2013-02-02: Aldone se Ctrl aux Alt estas premataj, mi volas fari nenion
        if((GetKeyState(VK_CONTROL) & 0x80) || (GetKeyState(VK_MENU) & 0x80))
            return CallNextHookEx(kbdHook, code, wParam, lParam);

        if(wParam == WM_KEYDOWN) {

            HWND foregroundWin = GetForegroundWindow();
            /* Aktualigo en 2011-05-30: La auxtoro de Tajpi faris
            korekton en sia programo en la versio 2.88, cxar
            la sekva linio ne suficxas por iuj multfadenaj programoj.
            Mi retradukis liajn sxangxojn, vidu sube. */
            //HKL layout = GetKeyboardLayout(GetWindowThreadProcessId(foregroundWin, 0));

            DWORD remoteThreadId = GetWindowThreadProcessId(foregroundWin, 0);
            DWORD currentThreadId = GetCurrentThreadId();
            AttachThreadInput(remoteThreadId, currentThreadId, TRUE);
            HWND focusedWindow = GetFocus();
            AttachThreadInput(remoteThreadId, currentThreadId, FALSE);
            HKL layout = GetKeyboardLayout(GetWindowThreadProcessId(focusedWindow, 0));

            UINT map = MapVirtualKeyEx(hookStruct->vkCode, MAPVK_VK_TO_CHAR, layout);
            wchar_t signo = map & 0xffff;

            switch(signo) {
                case L'C': case L'G':
                case L'H': case L'J':
                case L'S': case L'U':
                    bufro = CxuMajuskle() ? signo : SimplaMinuskligo(signo);
                    break;

                case L'X':
                    switch(bufro) {
                        case L'C': case L'c':
                        case L'G': case L'g':
                        case L'H': case L'h':
                        case L'J': case L'j':
                        case L'S': case L's':
                        case L'U': case L'u':
                            UninstallHook();
                            bufro = KonvertiEo(bufro);
                            SendiEo(bufro);
                            InstallHook();
                            return 1;

                        // 2013-02-02: malkonverti kiam oni tajpas ikson dufoje
                        case CX: case cx:
                        case GX: case gx:
                        case HX: case hx:
                        case JX: case jx:
                        case SX: case sx:
                        case UX: case ux:
                            UninstallHook();
                            MalsendiEo(MalkonvertiEo(bufro), CxuMajuskle() ? L'X' : L'x');
                            bufro = 0;
                            InstallHook();
                            return 1;
                    }
                    bufro = 0;
                    break;

                default:
                    bufro = 0;
                    break;
            }
        }
    } else {
        bufro = 0;
    }
    return CallNextHookEx(kbdHook, code, wParam, lParam);
}

// Vi povas elekti la normalan wWinMain...
int APIENTRY wWinMain(HINSTANCE hInstance, HINSTANCE, LPTSTR, int)
{
    hInst = hInstance;
    backScan = MapVirtualKey(VK_BACK, MAPVK_VK_TO_VSC);
    InstallHook();

    MSG msg;
    while(GetMessage(&msg, NULL, 0, 0)) {
        DispatchMessage(&msg);
    } // Por fini la programon, simple mortigu gxin, per la administrilo de taskoj :-)

    UninstallHook();
    return (int) msg.wParam;
}

// ... aux vi povas agordi vian kompililon kaj "ligilon" (linker)
// por ne uzi la defauxltajn bibliotekojn de C kaj C++ kaj tiukaze
// ankaux agordu ilin por komenci la programon per cxi tiu funkcio:
/*
DWORD CALLBACK SimplaEntryPoint()
{
    // http://blogs.msdn.com/b/oldnewthing/archive/2011/05/25/10168020.aspx
    hInst = GetModuleHandle(0);
    backScan = MapVirtualKey(VK_BACK, MAPVK_VK_TO_VSC);
    InstallHook();

    MSG msg;
    while(GetMessage(&msg, NULL, 0, 0)) {
        DispatchMessage(&msg);
    } // Por fini la programon, simple mortigu gxin, per la administrilo de taskoj :-)

    // Tion ni farus se ekzistus klavkombino por fini la programon.
    UninstallHook();
    ExitProcess(0);
    return 0;
}
*/
