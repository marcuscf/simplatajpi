SimplaTajpi
===========

Simpligita versio de la konata programo Tajpi, kiu ebligas tajpi Esperantajn
literojn en Windows.

Tajpi estas libera softvaro, eldonata kun sia fontkodo laŭ la kondiĉoj de la
GNU GPL versio 3. [La retejo de Tajpi estas ĉi tie](http://www.zz9pza.net/tajpi/)

Do, SimplaTajpi ankaŭ estas libere havebla laŭ GNU GPL versio 3. Tajpi estas
verkita en Visual Basic, kaj SimplaTajpi estas verkita en la programlingvo C++,
por esti pli portebla. Eblas kompili ĝin per mingw-w64, aŭ MS VC++ 2010 Express
(kaj probable per pli novaj versioj).

Se vi volas uzi mingw-w64, la jena komando sufiĉos:

g++ SimplaTajpi.cpp -o SimplaTajpi.exe -O2 -s -mwindows -municode -DWINVER=0x0400

Se vi volas uzi MS VC++ Express, sufiĉas krei unikodan projekton por programo
de Windows kaj enmeti la fontokodon en ĝi.
